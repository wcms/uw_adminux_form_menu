/**
 * @file
 * Adds a checkbox for enabling/disabling the menu item when adding/editing a node.
 *
 * Do this because a node needs to be added to a menu to display proper breadcrumbs.
 */

(function ($) {

  Drupal.behaviors.menuFieldsetSummaries = {
    attach: function (context) {
      $('fieldset.menu-link-form', context).drupalSetSummary(function (context) {
        summary = '';
        // Check if in site hierarchy, and set text.
        if ($('#edit-menu-enabled', context).is(':checked')) {
          summary += 'Parent: ' + Drupal.checkPlain($('#edit-menu-parent option:selected', context).text());
        }
        else {
          return Drupal.t('Not in hierarchy<br />Not in menu');
        }
        // Check if menu link is enabled.
        if ($('#edit-menu-hidden', context).is(':checked')) {
          summary += '<br />Menu: ' + Drupal.checkPlain($('#edit-menu-link-title', context).val());
        }
        else {
          summary += '<br />' + Drupal.t('Not in menu');
        }
        return summary;
      });
    }
  };

  Drupal.behaviors.menuLinkDisableAddMenuLink = {
    attach: function (context) {
      $('#edit-menu-parent', context).change(function () {
        if ($('#edit-menu-parent option:selected', context).text().indexOf('(not in menu)') != -1) {
          $('#edit-menu-hidden', context).removeAttr('checked').trigger('change').attr('disabled', 'disabled');
        }
        else {
          $('#edit-menu-hidden', context).removeAttr('disabled');
        }
      });
    }
  };

})(jQuery);
